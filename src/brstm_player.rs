#[allow(unused)]
use crate::brstm::BrstmFile;
use libmpv::{FileState, Mpv};
use std::error::Error;

#[allow(dead_code)]
pub struct BrstmPlayer {
    brstm: BrstmFile,
    path: String,
    mpv: Mpv,
}

impl BrstmPlayer {
    pub fn setup(&self) -> Result<(), String> {
        let mpv = &self.mpv;
        let mut loop_str: Option<String> = None;
        if self.brstm.loop_flag() {
            loop_str = Some(format!(
                "ab-loop-a={},ab-loop-b={},ab-loop-count=inf",
                self.brstm.loop_start_time(),
                self.brstm.duration()
            ));
        }
        if let Err(e) = mpv.playlist_load_files(&[(
            self.path.as_str(),
            FileState::Replace,
            loop_str.as_deref(),
        )]) {
            return Err(format!("ERROR: could not open \"{}\" ({e})", self.path));
        }

        Ok(())
    }
    pub fn seek(&mut self, secs: f64) -> Result<(), Box<dyn Error>> {
        self.mpv.seek_absolute(secs)?;
        Ok(())
    }
}

impl TryFrom<&str> for BrstmPlayer {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let brstm = BrstmFile::try_from(value)?;
        let mpv = Mpv::new();
        match mpv {
            Err(e) => Err(format!("ERROR: could not start mpv ({e})")),
            Ok(mpv) => Ok(BrstmPlayer {
                brstm,
                path: value.to_string(),
                mpv,
            }),
        }
    }
}

impl TryFrom<String> for BrstmPlayer {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let brstm = BrstmFile::try_from(value.as_str())?;
        let mpv = Mpv::new();
        match mpv {
            Err(e) => Err(format!("ERROR: could not start mpv ({e})")),
            Ok(mpv) => Ok(BrstmPlayer {
                brstm,
                path: value.to_string(),
                mpv,
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::BrstmPlayer;
    use std::{error::Error, thread, time::Duration};

    const TEST_FILE: &str = "./assets/DontDoIt.brstm";

    #[test]
    fn test_mpv_setup() -> Result<(), Box<dyn Error>> {
        let brstm = BrstmPlayer::try_from(TEST_FILE)?;
        brstm.setup()?;

        thread::sleep(Duration::from_secs(35));

        Ok(())
    }
}
